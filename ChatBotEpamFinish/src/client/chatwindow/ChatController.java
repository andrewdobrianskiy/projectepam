package client.chatwindow;


import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;

import javafx.scene.text.TextFlow;


import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.ResourceBundle;


public class ChatController implements Initializable {

    @FXML
    public TextArea messageBox;
    @FXML
    private Label usernameLabel;
    @FXML
    public VBox chatPane;
    @FXML
    private Button btnEmoji;
    @FXML
    private TextFlow emojiList;
    @FXML
    private AnchorPane titleBar;
    @FXML
    public Button buttonSend;
    @FXML
    ScrollPane scrollPane;
    @FXML
    private BorderPane rootPane;
    @FXML
    private AnchorPane chatPaneGeneral;
    @FXML
    ImageView microphoneImageView;
    @FXML
    private Button recordBtn;


    ImageView userImageView = new ImageView();
    private PreparedStatement pst = null;
    private Connection connection = null;
    private ResultSet rs = null;
    private ObservableList<HBox> dataMessage;


    final KeyCombination keyComb1 = new KeyCodeCombination(KeyCode.ENTER,
            KeyCombination.CONTROL_DOWN);

    private double xOffset;
    private double yOffset;




    @FXML
    public void sendButtonAction() throws IOException {
        if (messageBox.getText().trim().equals("")) return;
        String msg = messageBox.getText();

                Conversate conversate = new Conversate(msg);

        messageBox.clear();
        messageBox.requestFocus();


    }















    public void sendMethod(KeyEvent event) throws IOException {
        if (event.getCode() == KeyCode.ENTER) {
            sendButtonAction();
        }
    }

    @FXML
    public void closeApplication() {
        Platform.exit();
        System.exit(0);
    }



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        recordBtn.setVisible(false);
        dataMessage = FXCollections.observableArrayList();
        chatPane.setFocusTraversable(false);
        buttonSend.setTooltip(new Tooltip("Відправити"));

        scrollPane.vvalueProperty().bind(chatPane.heightProperty());


        titleBar.setOnMousePressed(event -> {
            xOffset = event.getSceneX();
            yOffset = event.getSceneY();
        });

        titleBar.setOnMouseDragged(event -> {
            titleBar.getScene().getWindow().setX(event.getScreenX() - xOffset);
            titleBar.getScene().getWindow().setY(event.getScreenY() - yOffset);
        });
        messageBox.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyComb1.match(keyEvent)) {
                    try {
                        sendButtonAction();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
        });

    }

}